import java.util.ArrayList;
import java.util.List;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.FileWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.Collections;
import java.util.Comparator;


public class InsuranceCSVReader 
{

	public static void main(String[] args) 
	{
		List<InsuranceEnrolleeData> insuranceCompany = insuranceCSVData("/Users/jameswhite/AvailityExercises/InsuranceCompanyData.txt");
		
		List<InsuranceCompanyList> insuranceNameList = new ArrayList<>();
		//use the entire data file to find all insurance companies
	    for( int i = 0; i != insuranceCompany.size(); i++ )
	    { 
	    	InsuranceEnrolleeData insCompany = insuranceCompany.get(i);
	        String companyName = insCompany.getInsCompany();
            
			InsuranceCompanyList insCompanyList = new InsuranceCompanyList(companyName);
			insCompanyList.setInsCompany( insCompany.getInsCompany() );
	        if( insuranceNameList.isEmpty() )
        	{
                insuranceNameList.add( insCompanyList );
            }
            else
            {
	        	for( InsuranceCompanyList companyList : insuranceNameList )
		        {
		        	if( !companyList.getInsCompany().contains( companyName ) )
		        	{
						insuranceNameList.add( insCompanyList );	
						break;
		        	}
		        }	
        	}
	    }
		
	    //loop through the list of insurance companies
	    for( InsuranceCompanyList companyList : insuranceNameList )
	    {
	    	//list of all enrollees
	    	List<InsuranceEnrolleeData> enrollees = new ArrayList<>();
	    	//loop through the entire CSV list
	    	for( InsuranceEnrolleeData insCompany : insuranceCompany )
			{
				//if the insCompany matches add to list
				if( insCompany.getInsCompany().equals( companyList.getInsCompany() ) )
				{
					//only include the one with the latest version
					if( enrollees.isEmpty() )
					{
						enrollees.add( insCompany );
					}
					else
					{
						//this loop will take all items in the list being created and verify whether or not
						//a userId already exists and check the version number.
						for( InsuranceEnrolleeData checkVersion : enrollees )
						{
							if( checkVersion.getUserId().equals( insCompany.getUserId() ) && 
									insCompany.getVersion() > checkVersion.getVersion() )
							{
								enrollees.remove( checkVersion );
							}
						}
						enrollees.add( insCompany );	
					}
				}
			}
	    	
	    	//create new CSV file with the enrollees list and name it using the company name
	        //sort by name asc
			
			Collections.sort(enrollees, new NameComparator() );

			String csvFile = "/Users/jameswhite/AvailityExercises/" + companyList.getInsCompany() + ".txt";

			try ( PrintWriter csvWriter = new PrintWriter( new FileWriter( csvFile ) ) )
			{
				for(InsuranceEnrolleeData item : enrollees)
				{
					csvWriter.print( item.getUserId() );
					csvWriter.print(",");
					csvWriter.print( item.getFirstLastName() );
					csvWriter.print(",");
					csvWriter.print( item.getVersion() );
					csvWriter.print(",");
					csvWriter.println( item.getInsCompany() );
				}
			} catch ( Exception e ) 
			{
				//Handle exception
				e.printStackTrace();
			}
	    }
	}

	public static List<InsuranceEnrolleeData> insuranceCSVData(String csvFile) 
	{
		List<InsuranceEnrolleeData> insuranceCompanies = new ArrayList<>();
		Path filePath = Paths.get( csvFile );
		 
		try( BufferedReader reader = Files.newBufferedReader( filePath, StandardCharsets.US_ASCII ) )
		{
			String csvLine = reader.readLine();
			
			while( csvLine != null  )
			{
				String[] csvLineData = csvLine.split( "," );
				
				InsuranceEnrolleeData insCompany = createInsCompany( csvLineData );
				
				insuranceCompanies.add( insCompany );
				
				csvLine = reader.readLine(); 
			}
		}
		catch (Exception e) 
		{
			System.out.println("CSV File not readable");
		}
		
		return insuranceCompanies;
	}
	
	private static InsuranceEnrolleeData createInsCompany( String[] data )
	{
		String userId = data[0];
		String firstLastName = data[1];
		int version = Integer.parseInt(data[2]);
		String insCompany = data[3];
		
		return new InsuranceEnrolleeData(userId,firstLastName,version,insCompany);
	}

}
class NameComparator implements Comparator<InsuranceEnrolleeData>
{
	public int compare(InsuranceEnrolleeData a, InsuranceEnrolleeData b) 
	{
		return a.getFirstLastName().compareTo( b.getFirstLastName() );
	}
}
class InsuranceEnrolleeData 
{
	private String userId;
	private String firstLastName;
	private int version;
	private String insCompany;
	
	public InsuranceEnrolleeData(
			String userId,
			String firstLastName,
			int version,
			String insCompany )
	{
		this.userId = userId;
		this.firstLastName = firstLastName;
		this.version = version;
		this.insCompany = insCompany;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getFirstLastName() {
		return firstLastName;
	}

	public void setFirstLastName(String firstLastName) {
		this.firstLastName = firstLastName;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getInsCompany() {
		return insCompany;
	}

	public void setInsCompany(String insCompany) {
		this.insCompany = insCompany;
	}
}

class InsuranceCompanyList
{
	private String insCompany;
	
	public InsuranceCompanyList(
			String insCompany )
	{
		this.insCompany = insCompany;
	}

	public String getInsCompany() {
		return insCompany;
	}

	public void setInsCompany(String insCompany) {
		this.insCompany = insCompany;
	}
}