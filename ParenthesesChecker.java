import java.util.ArrayDeque;

public class ParenthesesChecker

{
       static boolean areParenthesesClosed( String lispCode )
       {
              ArrayDeque<Character> lispChecker = new ArrayDeque<>();

              for( int i = 0; i < lispCode.length(); i++ )
              {
                     char x = lispCode.charAt(i);

                     //validate if the first character is an opening parenthesis
                     if( x == '(' )
                     {
                           lispChecker.push(x);
                           continue;
                     }

                     //if empty then the first character is not an opening parenthesis
                     if( lispChecker.isEmpty() )
                     {
                           return false;
                     }

                     if( !lispChecker.isEmpty() && x == ')' )
                     {
                           lispChecker.pop();
                     }
              }

              return ( lispChecker.isEmpty() );
       }

    public static void main(String[] args)
    {
        String lispCode = "(+ (* (/ 9 5) 60) 32)";

        if  ( areParenthesesClosed( lispCode ) )
        {
             System.out.println("LISP code properly formatted");
        }
        else
        {
             System.out.println("Parenthesis Error in LISP code");     
        }
    }
}